#include <iostream>
#include <sstream>
#include <string>

int main() {
    {
        /**
         * Task 1
         * Написать программу, проверяющую что сумма двух (введенных с клавиатуры) чисел
         * лежит в пределах от 10 до 20 (включительно), если да – вывести
         * строку "true", в противном случае – "false";
         */
        int a, b;
        std::cout << "Enter number:";
        std::cin >> a;
        std::cout << "Enter number:";
        std::cin >> b;
        std::cout << (((a + b) >= 10 && (a + b) <= 20) ? "true" : "false") << std::endl;
    }

    {
        /**
         * Task 2
         * Написать программу, выводящую на экран строку “true”,
         * если две целочисленные константы, объявленные в её начале либо обе равны десяти сами по себе,
         * либо их сумма равна десяти. Иначе "false".
         */
        const int a = 10, b = 7;
        std::cout << (((a == 10 && b == 10) || (a + b) == 10) ? "true" : "false") << std::endl;
    }

    {
        /**
         * Task 3
         * Написать программу которая выводит на экран список всех нечетных чисел он 1 до 50.
         * Например: "Your numbers: 1 3 5 7 9 11 13 …". Для решения используйте любой С++ цикл.
         */
        std::cout << "Your numbers: ";
        for (int i = 1; i <= 50; i += 2) {
            std::cout << i << " ";
        }
        std::cout << std::endl;
    }

    {
        /**
         * Task 4
         * Написать программу, проверяющую, является ли некоторое число - простым.
         * Простое число — это целое положительное число, которое делится без остатка только на единицу и себя само.
         */
        // Число которое мы проверяем на простоту.
        size_t a;
        std::string s_simple;
        // Ряд знаменитых учёных Древней Греции рассматривали каждое из натуральных чисел как собрание единиц; сама же единица числом не считалась.
        std::cout << "Enter number more than one:";
        do {
            std::cin >> a;
        } while (a < 2);
//        for (a = 2; a < 200; a++) {
        // Число 2 является простым, а все чётные нет.
        bool b_is_simple = (a == 2 || (a > 2 && (a % 2) != 0));
        size_t divider = 2;
        // Проверяем деление на все нечётные числа.
        for (size_t i = 3; i < (a / 2); i += 2) {
            if ((a % i) == 0) {
                b_is_simple = false;
                divider = i;
                break;
            }
        }
        std::ostringstream ss;
        ss << a;
        std::ostringstream ss1;
        ss1 << divider;
        s_simple = "Number " + ss.str() + " is ";
        std::cout << s_simple + (b_is_simple ? "simple" : "complex. Divider is " + ss1.str()) << std::endl;
//        }
    }

    {
        /**
         * Task 5
         * Пользователь вводит с клавиатуры число (год): от 1 до 3000.
         * Написать программу, которая определяет является ли этот год високосным.
         * Каждый 4-й год является високосным, кроме каждого 100-го, при этом каждый 400-й – високосный.
         * Вывести результаты работы программы в консоль.
         */
        size_t year;
        std::cout << "Enter year less than three thousand:";
        do {
            std::cin >> year;
        } while (year > 3000);
        bool b_is_leap_year;
        if (!(year % 400)) {
            b_is_leap_year = true;
        } else if (!(year % 100)) {
            b_is_leap_year = false;
        } else if (!(year % 4)) {
            b_is_leap_year = true;
        } else {
            b_is_leap_year = false;
        }
        std::cout << (b_is_leap_year ? "Leap year" : "Common year") << std::endl;
    }
    return 0;
}
